 
import './Contact.css';

export default function Contact(props) {
    let dotClass ="";

    if (props.onLine) {
        dotClass ="onLine";
    }
      return (
        <div className="contact">
          <div className="contenu">
              <img className="image" src={props.contactImage}></img>
              <div className={"profil_connection " + dotClass}></div>  
          </div>
        <p className="name">{props.contactName}</p>
        </div>
      )
}

