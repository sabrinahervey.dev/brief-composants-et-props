import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Contact from "./components/Contact"
function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Page contact</h1>
      <div className="card">
      </div>
      <Contact 
      contactName="Katherine Johson"
      contactImage="portrait1.jpg"
      onLine={true}
      />
      <Contact 
      contactName="Mickael Dupont"
      contactImage="portrait2.jpg"
      onLine={false}
      />
    </>
  )
}

export default App
